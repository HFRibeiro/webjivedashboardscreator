# WebjiveDashboardsCreator


## Install on Linux:

```
sudo apt-get -y install g++ qt5-default libqt5widgets5* libqt5webkit5*
cd webjivedashboardscreator
qmake
make
./Webjivedashboardscreator
```


### Comand to import dashboard

```
docker exec -i mongodb mongoimport --db dashboards --collection dashboards < data/mongo/dashboards.json
```

### Comand to export dashboard

```
docker exec -i mongodb mongoexport --db dashboards --collection dashboards > data/mongo/dashboards.json
```