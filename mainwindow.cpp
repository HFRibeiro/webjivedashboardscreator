#include "mainwindow.h"
#include "ui_mainwindow.h"

/*
{"_id":{"$oid":"5d84f57d1a3e440016c7af74"},"widgets":[
{"_id":{"$oid":"5d84f5831a3e440016c7af75"},"id":"1","x":0,"y":0,"canvas":"0","width":2,"height":2,"type":"LED_DISPLAY","inputs":
{"attribute":{"device":"sys/tg_test/1","attribute":"double_scalar"},"relation":"\u003e","compare":500,"classColor":"red-led","showDevice":false,"showValue":true}},
{"_id":{"$oid":"5d84f59c1a3e440016c7af7e"},"id":"2","x":0,"y":2,"canvas":"0","width":2,"height":2,"type":"LED_DISPLAY","inputs":
{"attribute":{"device":"sys/tg_test/1","attribute":"double_scalar"},"relation":"\u003e","compare":500,"classColor":"red-led","showDevice":false,"showValue":true}}
],"insertTime":{"$date":"2019-09-20T15:51:25.433Z"},"updateTime":{"$date":"2019-09-20T15:54:12.460Z"},"user":"user1","name":"TypeWidget","__v":24}
*/

#define MAX_WIDGETS_ROW 23

QStringList typesOfData = {"double_scalar","short_scalar","float_scalar","long_scalar","long64_scalar"};
QStringList typesOfDataSpectrum = {"double_spectrum","short_spectrum","float_spectrum","long_spectrum"};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->cb_type->addItem("LED");
    ui->cb_type->addItem("SPECTRUM");
    ui->cb_type->addItem("ATTRIBUTE_DISPLAY");
    ui->cb_type->addItem("ATTRIBUTE_WRITER");
    ui->cb_type->addItem("ATTRIBUTE_PLOT");
    ui->cb_type->addItem("ATTRIBUTE_SCATTER");
    ui->cb_type->addItem("ATTRIBUTE_DIAL");
    ui->cb_type->addItem("ATTRIBUTE_LOGGER");
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString GetRandomString()
{
   const QString possibleCharacters("abcdef0123456789");
   const int randomStringLength = 24; // assuming you want random strings of 24 characters

   QString randomString;
   for(int i=0; i<randomStringLength; ++i)
   {
       int index = qrand() % possibleCharacters.length();
       QChar nextChar = possibleCharacters.at(index);
       randomString.append(nextChar);
   }
   return randomString;
}


void MainWindow::on_bt_create_clicked()
{
    int nWidgets = ui->nb_widgets->value();
    QString JsonWidget = "{\"_id\":{\"$oid\":\""+ui->tb_widgetID->text()+"\"},\"widgets\":[";
    int x = 0;
    int y = 0;
    int countRow = 0;

    for(int i=0; i<nWidgets; i++)
    {

        if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "LED")
        {
            int randNum = rand() % typesOfData.length() + 0;

            JsonWidget += "{\"_id\":{\"$oid\":\""+GetRandomString()+"\"},\"id\":\""+QString::number(i+1)+
                    "\",\"x\":"+QString::number(x)+",\"y\":"+QString::number(y)+
                    ",\"canvas\":\"0\",\"width\":2,\"height\":2,\"type\":\"LED_DISPLAY\",\"inputs\":{\"attribute\":{\"device\":\"sys/tg_test/1\",\"attribute\":\""+
                    typesOfData[randNum]+"\"},\"relation\":\"\u003e\",\"compare\":500,\"classColor\":\"red-led\",\"showDevice\":false,\"showValue\":true}}";
        }
        else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "SPECTRUM")
        {
            int randNum = rand() % typesOfDataSpectrum.length() + 0;
            JsonWidget += "{\"_id\":{\"$oid\":\""+GetRandomString()+"\"},\"id\":\""+QString::number(i+1)+
                    "\",\"x\":"+QString::number(x)+",\"y\":"+QString::number(y)+
                    ",\"canvas\":\"0\",\"width\":17,\"height\":10,\"type\":\"SPECTRUM\",\"inputs\":{\"attribute\":{\"device\":\"sys/tg_test/1\",\"attribute\":\""+
                    typesOfDataSpectrum[randNum]+"\"},\"showTitle\":true,\"inelastic\":false}}";
        }
        else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_DISPLAY")
        {
            int randNum = rand() % typesOfData.length() + 0;
            JsonWidget += "{\"_id\":{\"$oid\":\""+GetRandomString()+"\"},\"id\":\""+QString::number(i+1)+
                    "\",\"x\":"+QString::number(x)+",\"y\":"+QString::number(y)+
                    ",\"canvas\":\"0\",\"width\":10,\"height\":2,\"type\":\"ATTRIBUTE_DISPLAY\",\"inputs\":{\"attribute\":{\"device\":\"sys/tg_test/1\",\"attribute\":\""+
                    typesOfData[randNum]+"\"},\"precision\":2,\"showDevice\":false}}";
        }
        else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_WRITER")
        {
            int randNum = rand() % typesOfData.length() + 0;
            JsonWidget += "{\"_id\":{\"$oid\":\""+GetRandomString()+"\"},\"id\":\""+QString::number(i+1)+
                    "\",\"x\":"+QString::number(x)+",\"y\":"+QString::number(y)+
                    ",\"canvas\":\"0\",\"width\":5,\"height\":2,\"type\":\"ATTRIBUTE_WRITER\",\"inputs\":{\"attribute\":{\"device\":\"sys/tg_test/1\",\"attribute\":\""+
                    typesOfData[randNum]+"\"},\"showDevice\":false,\"showDevice\":false}}";
        }
        else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_PLOT")
        {
            int randNum = rand() % typesOfData.length() + 0;
            JsonWidget += "{\"_id\":{\"$oid\":\""+GetRandomString()+"\"},\"id\":\""+QString::number(i+1)+
                    "\",\"x\":"+QString::number(x)+",\"y\":"+QString::number(y)+
                    ",\"canvas\":\"0\",\"width\":12,\"height\":9,\"type\":\"ATTRIBUTE_PLOT\",\"inputs\":{\"timeWindow\":120,\"showZeroLine\":true,\"attributes\":[{\"attribute\":{\"device\":\"sys/tg_test/1\",\"attribute\":\""+
                    typesOfData[randNum]+"\"},\"yAxis\":\"left\"}]}}";
        }
        else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_SCATTER")
        {
            int randNum = rand() % typesOfData.length() + 0;
            int randNum2 = rand() % typesOfData.length() + 0;
            JsonWidget += "{\"_id\":{\"$oid\":\""+GetRandomString()+"\"},\"id\":\""+QString::number(i+1)+
                    "\",\"x\":"+QString::number(x)+",\"y\":"+QString::number(y)+
                    ",\"canvas\":\"0\",\"width\":12,\"height\":9,\"type\":\"ATTRIBUTE_SCATTER\",\"inputs\":{\"independent\":{\"device\":\"sys/tg_test/1\",\"attribute\":\""+
                    typesOfData[randNum]+"\"},\
\"dependent\":{\"device\":\"sys/tg_test/1\",\"attribute\":\""+
                    typesOfData[randNum2]+"\"}}}";
        }
        else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_DIAL")
        {
            int randNum = rand() % typesOfData.length() + 0;
            JsonWidget += "{\"_id\":{\"$oid\":\""+GetRandomString()+"\"},\"id\":\""+QString::number(i+1)+
                    "\",\"x\":"+QString::number(x)+",\"y\":"+QString::number(y)+
                    ",\"canvas\":\"0\",\"width\":4,\"height\":4,\"type\":\"ATTRIBUTE_DIAL\",\"inputs\":{\"attribute\":{\"device\":\"sys/tg_test/1\",\"attribute\":\""+
                    typesOfData[randNum]+"\"},\"min\":0,\"max\":1000,\"label\":\"attribute\",\"showWriteValue\":true}}";
        }
        else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_LOGGER")
        {
            int randNum = rand() % typesOfData.length() + 0;
            JsonWidget += "{\"_id\":{\"$oid\":\""+GetRandomString()+"\"},\"id\":\""+QString::number(i+1)+
                    "\",\"x\":"+QString::number(x)+",\"y\":"+QString::number(y)+
                    ",\"canvas\":\"0\",\"width\":17,\"height\":11,\"type\":\"ATTRIBUTE_LOGGER\",\"inputs\":{\"attribute\":{\"device\":\"sys/tg_test/1\",\"attribute\":\""+
                    typesOfData[randNum]+"\"},\"linesDisplayed\":100,\"showDevice\":true,\"logIfChanged\":false}}";
        }


        if(i<nWidgets-1) JsonWidget += ",";
        if(countRow < ui->nb_row->value()-1)
        {
            if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "LED") y = y+2;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "SPECTRUM")  y = y+10;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_DISPLAY")  y = y+2;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_WRITER")  y = y+2;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_PLOT")  y = y+9;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_SCATTER")  y = y+9;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_DIAL")  y = y+4;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_LOGGER")  y = y+11;
            countRow++;
        }
        else
        {
            if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "LED") x = x+2;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "SPECTRUM")  x = x+17;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_DISPLAY")  x = x+10;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_WRITER")  x = x+5;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_PLOT")  x = x+12;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_SCATTER")  x = x+12;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_DIAL")  x = x+4;
            else if(ui->cb_type->itemText(ui->cb_type->currentIndex()) == "ATTRIBUTE_LOGGER")  x = x+17;
            countRow = 0;
            y = 0;
        }
    }
    JsonWidget += "],\"insertTime\":{\"$date\":\"2019-09-20T15:51:25.433Z\"},\"updateTime\":{\"$date\":\"2019-09-20T15:54:12.460Z\"},\"user\":\"user1\",\"name\":\"benchmark\",\"__v\":24}";

    qDebug("%s",JsonWidget.toStdString().c_str()) ;

    QFile file(ui->tb_path->text());
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;

    QTextStream out(&file);
    out << JsonWidget.toStdString().c_str();
    file.close();

}
